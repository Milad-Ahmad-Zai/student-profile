﻿(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.profile', {
        url: '/profile',
        views: {
          '@': {
            templateUrl: 'src/app/profile/profile.tpl.html',
            controller: 'ProfileController'
          }
        }
      });
  }

  ProfileController.$inject = ['$scope', 'UserService', '$location', '$rootScope', 'FlashService', 'UploadService'];
  function ProfileController($scope, UserService, $location, $rootScope, FlashService, UploadService) {
      $scope.vm = this;

      $scope.vm.user = null;

      initController();

      function initController() {
          loadCurrentUser();
      }

      function loadCurrentUser() {
          UserService.GetByUsername($rootScope.globals.currentUser.username)
              .then(function (user) {
                  $scope.vm.user = user;
              });
      }

      $scope.vm.register = register;

      function register() {
          $scope.vm.dataLoading = true;
          UserService.Update($scope.vm.user)
              .then(function (response) {
                  FlashService.Success('Profile updated successfuly', true);
                  $location.path('#/login');
              });
      }

      $scope.fileSelected = false;
      $scope.upload = function(){
          $scope.processing = true;
          $scope.fileSelected = false;
          var file = $scope.files;
          var url = 'http://localhost/projects/profile/server/upload.php';
          UploadService.upload(file, url)
          .then(function(response){
              if(response.data.success){
                $scope.vm.user.pic = response.data.file;
                $scope.processing = false;
                UserService.Update($scope.vm.user)
                .then(function (response) {
                });
              }
            },function(error){
                $scope.processing = false;
                $scope.fileSelected = true;
                console.log(error);
            });
      }


      $scope.imageUpload = function(event){
        var files = event.target.files;
        var file = files[files.length-1];
        $scope.file = file;
        var reader = new FileReader();
        reader.onload = $scope.imageIsLoaded;
        reader.readAsDataURL(file);
        $scope.fileSelected = true;
      }

      $scope.imageIsLoaded = function(e){
        $scope.$apply(function(){
          $scope.step = e.target.result;
        });
      }

  }

  angular.module('profile', [])
    .config(config)
    .controller('ProfileController', ProfileController);
})();
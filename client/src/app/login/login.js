﻿(function() {
  'use strict';

  /**
   * @name  config
   * @description config block
   */
  function config($stateProvider) {
    $stateProvider
      .state('root.login', {
        url: '/login',
        views: {
          '@': {
            templateUrl: 'src/app/login/login.tpl.html',
            controller: 'LoginController'
          }
        }
      });
  }

  /**
   * @name  LoginController
   * @description Controller
   */
  function LoginController($log, $scope, $location, AuthenticationService, FlashService) {
        $log.debug('LoginController Loaded!');

        $scope.vm = this;
        $scope.vm.login = login;


        (function initController() {
            // reset login status
            AuthenticationService.ClearCredentials();
        })();

        function login() {
            $scope.vm.dataLoading = true;
            AuthenticationService.Login($scope.vm.username, $scope.vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials($scope.vm.username, $scope.vm.password);
                    $location.path('/');
                } else {
                    FlashService.Error(response.message);
                    $scope.vm.dataLoading = false;
                }
            });
        };
    }

  angular.module('login', [])
    .config(config)
    .controller('LoginController', LoginController);
})();

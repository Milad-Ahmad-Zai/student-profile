﻿(function () {
    'use strict';

    angular
        .module('common.services.upload', [])
        .factory('UploadService', UploadService);

    UploadService.$inject = ['$http'];
    function UploadService($http) {
        
        var service = {};

        service.upload = upload;
        return service;

        function upload(files, url){
            var fd = new FormData();
            angular.forEach(files, function(file){
              fd.append('file', file);
            });
            
            var request = $http({
              method : 'POST',
              url : url,
              data : fd,
              transformRequest : angular.identity,
              headers: {'Content-Type':undefined}
            });
            return request;
        }

    }

})();
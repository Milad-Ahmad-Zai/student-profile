### Getting started

A complete AngularJS student profile app, with login and registration.

Install **node.js**. Then **gulp** and **bower** if you haven't yet.

    $ npm -g install gulp bower

After that, Unzip the project and cd into it, then install bower and npm dependencies, and run the application in development mode.

    $ npm install
    $ bower install
    $ gulp serve

You are now ready to go, the applcation is available at **http://127.0.0.1:3000** with live reload.

You also need to modify you php server to serve the following serverside script file from localhost
	
	$ upload.php **inside server folder**

 then modify the path in...

 	$ client/src/app/profile/profile.js **line#56**
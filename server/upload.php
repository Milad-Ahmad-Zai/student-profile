<?php
	header('Content-Type: application/json');
	if(!empty($_FILES)){
		$tempPath = $_FILES['file']['tmp_name'];
		$uploadPath = "../client/assets/images/upload/" . $_FILES['file']['name'];
		$file = $_FILES['file']['name'];
		if(move_uploaded_file($tempPath, $uploadPath)){
			echo json_encode( ['success' => true, 'file' => $file] );
		}else{
			echo json_encode( ['success' => false, 'error' => 'file not uploaded!'] );
		}
	}else{
		echo json_encode( ['success' => false, 'error' => 'no file found!'] );
	}
?>